@extends('layouts.guest')

@section('content')

    <div id="admin-login">
        <div class="box">

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    ohhhhhhh! <br>
                    User not found
                </div>
            @endif

            <form action="{{ action('Admin\AccountController@auth') }}" method="post">
                @csrf

                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ old('name') }}"
                           class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                           placeholder="Name" autocomplete="on">
                    <small class="invalid-feedback">{{ $errors->first('name') }}</small>
                </div>

                <div class="form-group">
                    <label>Family</label>
                    <input type="text" name="family" value="{{ old('family') }}"
                           class="form-control {{ $errors->has('family') ? 'is-invalid' : '' }}"
                           placeholder="family" autocomplete="on">
                    <small class="invalid-feedback">{{ $errors->first('family') }}</small>
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value="{{ old('password') }}"
                           class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                           placeholder="Password">
                    <small class="invalid-feedback">{{ $errors->first('password') }}</small>
                </div>
                <br>
                <button class="btn btn-primary btn-block">Login</button>
            </form>
        </div>
    </div>


@endsection




