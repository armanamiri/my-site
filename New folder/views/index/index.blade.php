@extends('layouts.index')
@section('content')

    <img src="" alt="">
    <div class="section1">
        <div class="wrapper">

            <div>
                <ul>
                    <li><a href="#Home">ارتباط با ما</a></li>
                    <li><a href="#about Us">وبلاگ</a></li>
                    <li><a href="#Protfolio">کار های ما</a></li>
                    <li><a href="#Blog">درباره ما</a></li>
                    <li><a href="#Contact Us">خانه</a></li>
                </ul>
            </div>

            <div class="touch">
                <a href="">برقراری ارتباط</a>
            </div>

            <div class="spec">
                داده کاوان هوشمند
            </div>

        </div>
    </div>

    <div class="section2">
        <div class="wrapper1">
            <h1>
                خدمات شگفت انگیز
            </h1>
            <h3>
                طراحی مجموعه ای از صفحه ها , مشتری پسند و راحتی در استفاده
            </h3>

        </div>
    </div>

    <div class="section3">
        <div class="wrapper1">

            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo1.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo2.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo3.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo4.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo1.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
            <div style="display: flex; flex-direction: column; ">
                <img width="70px" height="70px" src="{{ asset('images/logo2.png') }}" alt="">
                <h4>
                    بازاریابی
                </h4>
                <h3>
                    دذلسل سدسدا سلدسلس سدسدسدس سدسددس سدلذلذ ذسذلذ ذلذل
                </h3>
            </div>
        </div>
    </div>

    <div class="section4">
        <div class="wrapper1">


            <div>
                <h1>تیمی پویا با خلاقیت نوین</h1>
                <h6>ذلسدلدس لذلذلذ لذلذففف ذلذلذلسذ لذسلذسلذ لسذ سلذسل سذذسذل سدسغدس غدسدغسد دغغادادا دایدیا ادادادا اد اد اداااد ادادادا ادادادالسدلدس لذلذلذ لذلذففف ذلذلذلسذ لذسلذسلذ لسذ سلذسل سذذسذل سدسغدس غدسدغسد دغغادادا دایدیا ادادادا اد اد اداااد ادادادا ادادادالسدلدس لذلذلذ لذ  </h6>
                <br><br><br> <a class="" href="">ادامه مطالب----></a>
            </div>

            <img src="{{ asset('images/p3.png') }}" alt="">
        </div>
    </div>

    <span>
        <div class="wrapper1">
            <h1>رزومه کاری عالی</h1>
            <h6>سففذسذ سذلذس ذلذسلذ سلذل لذسلذل لذلسذل سلذسلذ لذسلذ سلذسلذ لسذلذل </h6>
            <img src="{{ asset('images/p1.png') }}" alt="">
        </div>
    </span>

    <div class="section5">
        <div class="wrapper1">
            <h1>
                مراحل کار
            </h1>
            <h6>یریری یریری لفلفل فلفلف فلفلفلق قلبلق لف لفل فلفلفل فلفل ف.</h6>
            <img src="{{ asset('images/66.png') }}" alt="">
        </div>
    </div>

    <div class="section6">
        <div class="wrapper1">
        <h1>آخرین موضوعات</h1>
            <h6>بربر بربر بربربرب ربرب ربربرب ربربرب ربربرب ربربرب ربربرب ربربر بربرب ربرب ربر برب ر ب ربربرب </h6>
        </div>
    </div>

    <div class="section7">
        <div class="wrapper2">
        <div class="content1">1</div>
        <div class="content2">2</div>
        <div class="content3">3</div>
        </div>
    </div>

@endsection