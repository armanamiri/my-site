<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link type="text/css" rel="stylesheet" href="{{asset('css/fonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <title>Document</title>
</head>

<header id="header"></header>


<main id="main">


    <div class="content">
        @yield('content')

    </div>

    <div class="nav-bar">

        <div class="links">

            <a href="{{action('Admin\UserController@index')}}" class="link {{request()->is('users') ? 'active' : ''}}">

                <div class="icon">
                    <img src="{{ asset('icon/icon1.png') }}" style="margin-left: 4px">
                </div>
                <div class="text">
                    کاربران
                </div>
            </a>

            <a href="{{action('Admin\ProductController@index')}}" class="link {{request()->is('products') ? 'active' : ''}}">
                <div class="icon">
                    <img src="{{ asset('icon/icon2.png') }}" style="margin-left: 4px; margin-top: 2px;">
                </div>
                <div class="text">
                    محصولات
                </div>
            </a>

            <a href="{{action('Admin\CatController@index')}}" class="link {{request()->is('cats') ? 'active' : ''}}">
                <div class="icon">
                    <img src="{{ asset('icon/icon6.png') }}" style="margin-left: 9px; margin-top: 5px;">
                </div>
                <div class="text">
                    دسته بندی ها
                </div>
            </a>

            <a href="{{action('Admin\AccountController@login')}}" class="link">
                <div class="icon">
                    <img src="{{ asset('icon/icon6.png') }}" style="margin-left: 9px; margin-top: 5px;">
                </div>
                <div class="text">
                    خروج
                </div>
            </a>

        </div>

    </div>


</main>

<footer id="footer">
    @section('contentb')
    @show
</footer>
</body>
</html>