<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link type="text/css" rel="stylesheet" href="{{asset('css/fonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/guest.css') }}">

</head>
<body>
<main>
<header id="header"></header>

<main class="content">
    @yield('content')
</main>

<footer id="footer">Developed with <span>&hearts;</span> in Arman Lab - 2019</footer>
</main>
</body>
</html>