@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    مدیریت محصولات
                </div>
                <div class="toolbar">
                    <a href="{{ action('Admin\ProductController@new') }}" class="btn btn-success">درج کالای جدید</a>
                </div>
            </div>

            <div class="body">
                @if( session()->has('delete'))
                    <div class="alert alert-success">
                        succesful delete!
                    </div>
                @endif

                @if( session()->has('inserted'))
                    <div class="alert alert-success">
                        succesful inserted!
                    </div>
                @endif
                <table border="2px" class="table table-dark table-striped table-hover">
                    <tr>
                        <th>عملیات</th>
                        <th>تصویر</th>
                        <th>توضیحات</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
                        <th>دسته بندی</th>
                        <th>نام</th>
                        <th>#</th>
                    </tr>

                    @foreach( $records as $record)
                        <tr>
                            <td>
                                <a href="{{ action('Admin\ProductController@confirm' ,['id'=>$record->id] ) }}"
                                   class="btn btn-sm btn-danger">del</a>
                                <a href="{{ action('Admin\ProductController@edit' ,['id'=>$record->id] ) }}"
                                   class="btn btn-sm btn-warning">edit</a>
                            </td>
                            <td>
                                <img width="50px" height="50px " src="{{ asset('images/products/' . $record->image) }}" alt="">
                            </td>
                            <td>{{$record->description}}</td>
                            <td>{{$record->count}}</td>
                            <td>{{$record->price }}</td>
                            <td>
                               {{ $record->cat ? $record->cat->name : '-نامشخص-' }}
                            </td>

                            <td>{{$record->name}}</td>

                            <td>{{ $loop->index + $records->firstItem()}}</td>
                        </tr>
                    @endforeach

                </table>
                {{$records->links()}}

            </div>
        </div>
    </div>

@endsection