@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    درج محصولات
                </div>

                <div class="toolbar">

                </div>{{--خالی و بسته--}}

            </div>

            <div class="body">
                <form action="{{action('Admin\ProductController@insert')}}" method="post" enctype="multipart/form-data">
                    <div class="d-panel">
                        <div class="body">

                            @csrf
                            <div class="row">
                                <div class="col-4">
                                    <br>
                                    <img width="100%" height="250px" src="{{ asset('images/images.png') }}" alt="">
                                    <br>
                                    <br>
                                    <div class="form-row ">
                                        <label>تصویر</label>
                                        <input type="file"
                                               class="form-control-file frame-file-lg {{  $errors->has('image') ? 'is-invalid': '' }} "
                                               placeholder="image" name="image">
                                        <div class="invalid-feedback">
                                            {{$errors->first('image')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-8">


                                    <div class="form-row ">
                                        <label>دسته بندی</label>
                                        <select name="catId"
                                                class="form-control form-control-lg {{  $errors->has('catId') ? 'is-invalid' : '' }}">
                                            <option value="">انتخاب کنید</option>
                                            @foreach(App\Cat ::get() as $c)
                                                <option value="{{ $c->id }}" {{ $c->id== old('catId') ? 'selected' : '' }}>
                                                    {{ $c->name }}
                                                </option>
                                            @endforeach
                                        </select>

                                        <div class="invalid-feedback">
                                            {{$errors->first('catId')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>نام کالا</label>
                                        <input type="text"
                                               class="form-control form-control-lg  {{  $errors->has('name') ? 'is-invalid' : '' }}"
                                               placeholder="name" value="{{old('name')}}" name="name">
                                        <div class="invalid-feedback">
                                            {{$errors->first('name')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>قیمت</label>
                                        <input type="text"
                                               class="form-control  form-control-lg {{  $errors->has('price') ? 'is-invalid' : '' }} "
                                               placeholder="price" value="{{old('price')}}" name="price">
                                        <div class="invalid-feedback">
                                            {{$errors->first('price')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>تعداد</label>
                                        <input type="text"
                                               class="form-control  form-control-lg {{  $errors->has('count') ? 'is-invalid' : '' }}"
                                               placeholder=" count" value="{{old('count')}}" name="count">
                                        <div class="invalid-feedback">
                                            {{$errors->first('count')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>توضیحات</label>
                                        <input type="text"
                                               class="form-control form-control-lg {{  $errors->has('description') ? 'is-invalid' : '' }}"
                                               placeholder="description" value="{{old('description')}}"
                                               name="description">
                                        <div class="invalid-feedback">
                                            {{$errors->first('description')}}
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <br>

                        </div>
                    </div>
                    <div class="footer">
                        <div class="actions">
                            <button class="btn btn-success btn-lg">Save</button>
                            <a class="btn btn-light btn-lg"
                               href="{{ action('Admin\ProductController@index') }}">Back</a>
                            {{-- URL::previous() یا این یا اون بالایی--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection