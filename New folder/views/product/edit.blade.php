@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    ویرایش محصولات
                </div>

                <div class="toolbar">

                </div>
            </div>

            <div class="body">

                @if( session()->has('updated'))
                    <div class="alert alert-success">succesful updated!</div>
                @endif

                <form action="{{action('Admin\ProductController@update')}}" method="post" enctype="multipart/form-data">
                    <div class="d-panel">
                        <div class="body">

                            @csrf

                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{$record->id}}">


                            <div class="row">
                                <div class="col-4">
                                    <br>

                                    <img  width="100%" height="245px" src="{{ asset('images/products/' . $record->image) }}" alt="">
                                    <br>
                                    <br>

                                    <div class="form-row " >
                                        <label>تصویر</label>
                                        <input type="file"
                                               class="p-3 mb-2 bg-info text-white {{  $errors->has('image') ? 'is-invalid' : '' }}"
                                               placeholder="image" value="{{old('image'  , $record->image)}}"
                                               name="image">
                                        <div class="invalid-feedback">
                                            {{$errors->first('image')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-8">

                                    <div class="form-row ">
                                        <label>دسته بندی</label>
                                        <select name="catId"
                                                class="form-control form-control-lg {{  $errors->has('catId') ? 'is-invalid' : '' }}">
                                            <option value="">انتخاب کنید</option>


                                            @foreach(App\Cat ::get() as $c)
                                                <option value="{{ $c->id }}" {{ old('catId' , $record->catId) == $c->id ? 'selected' : '' }}>
                                                    {{ $c->name }}
                                                </option>
                                            @endforeach


                                        </select>

                                        <div class="invalid-feedback">
                                            {{$errors->first('catId')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>نام کالا</label>
                                        <input type="text"
                                               class="form-control form-control-lg  {{  $errors->has('name') ? 'is-invalid' : '' }}"
                                               placeholder="name" value="{{old('name'  , $record->name)}}" name="name">

                                        <div class="invalid-feedback">
                                            {{$errors->first('name')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>قیمت</label>
                                        <input type="text"
                                               class="form-control form-control-lg  {{  $errors->has('price') ? 'is-invalid' : '' }} "
                                               placeholder="price" value="{{old('price'  , $record->price)}}"
                                               name="price">

                                        <div class="invalid-feedback">
                                            {{$errors->first('price')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>تعداد</label>
                                        <input type="text"
                                               class="form-control form-control-lg  {{  $errors->has('count') ? 'is-invalid' : '' }}"
                                               placeholder=" count" value="{{old('count'  , $record->count)}}"
                                               name="count">
                                        <div class="invalid-feedback">
                                            {{$errors->first('count')}}
                                        </div>
                                    </div>

                                    <div class="form-row ">
                                        <label>توضیحات</label>
                                        <input type="text"
                                               class="form-control form-control-lg  {{  $errors->has('description') ? 'is-invalid' : '' }}"
                                               placeholder="description"
                                               value="{{old('description'  , $record->description)}}"
                                               name="description">
                                        <div class="invalid-feedback">
                                            {{$errors->first('description')}}
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                        <div class="footer">

                            <div class="actions">
                                <button class="btn btn-success btn-lg">save edit</button>
                                <a class="btn btn-light btn-lg" href="{{ action('Admin\ProductController@index') }}">Back</a>
                                {{-- URL::previous() یا این یا اون بالایی--}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection