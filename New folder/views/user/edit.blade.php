@extends('layouts.admin')

@section('content')

    <div id="user-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    ویرایش کاربران
                </div>
                <div class="toolbar"></div>
            </div>

            <div class="body">

                @if( session()->has('updated'))
                    <div class="alert alert-success">succesful updated!</div>
                @endif

                <form action="{{action('Admin\UserController@update')}}" method="post">
                    <div class="d-panel">
                        <div class="body">
                            @csrf

                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{$user->id}}">

                            <div class="form-row ">
                                <label>نام کاربر</label>
                                <input type="text" class="form-control {{  $errors->has('name') ? 'is-invalid' : '' }}"
                                       placeholder="name" value="{{old('name'  , $user->name)}}" name="name">
                                <div class="invalid-feedback">
                                    {{$errors->first('name')}}
                                </div>
                            </div>

                            <div class="form-row ">
                                <label>نام خانوادگی</label>
                                <input type="text"
                                       class="form-control {{  $errors->has('family') ? 'is-invalid' : '' }} "
                                       placeholder="family" value="{{old('family'  , $user->family)}}"
                                       name="family">
                                <div class="invalid-feedback">
                                    {{$errors->first('family')}}
                                </div>
                            </div>

                            <div class="form-row ">
                                <label>رمز</label>
                                <input type="text"
                                       class="form-control {{  $errors->has('password') ? 'is-invalid' : '' }}"
                                       placeholder=" password" value="{{old('password'  , $user->password)}}"
                                       name="password">
                                <div class="invalid-feedback">
                                    {{$errors->first('password')}}
                                </div>
                            </div>

                            <br>
                        </div>
                            <div class="footer">
                                <div class="actions">
                                    <button class="btn btn-success btn-lg">save edit</button>
                                    <a class="btn btn-light btn-lg"
                                       href="{{ action('Admin\UserController@index') }}">Back</a>
                                </div>
                            </div>


                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection