@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    مدیریت کاربران
                </div>
                <div class="toolbar">
                    <a href="{{ action('Admin\UserController@new') }}" class="btn btn-success">درج کاربر جدید</a>
                </div>
            </div>

            <div class="body">
                @if( session()->has('deleted'))
                    <div class="alert alert-success">succesful delete!</div>
                @endif

                    @if( session()->has('inserted'))
                        <div class="alert alert-success">succesful inserted!</div>
                    @endif

                <table border="2px" class="table table-dark table-striped table-hover">
                    <tr>

                        <th>عملیات</th>
                        <th>رمز</th>
                        <th>نام خانوادگی</th>
                        <th>نام</th>
                        <th>#</th>
                    </tr>

                    @foreach( $records as $user)
                        <tr>
                            <td>
                                <a href="{{ action('Admin\UserController@confirm' ,['id'=>$user->id] ) }}"
                                   class="btn btn-sm btn-danger">del</a>
                                <a href="{{ action('Admin\UserController@edit' ,['id'=>$user->id] ) }}"
                                   class="btn btn-sm btn-warning">edit</a>
                            </td>
                            <td>{{$user->password}}</td>
                            <td>{{$user->family}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{ $loop->index}}</td>
                        </tr>
                    @endforeach

                </table>
                {{$records->links()}}


            </div>
        </div>
    </div>

@endsection