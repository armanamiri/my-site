@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    درج کاربر
                </div>
                <div class="toolbar">

                </div>
            </div>

            <div class="body">


                @if( session()->has('inserted'))
                    <div class="alert alert-success">succesful inserted!</div>
                @endif


                <form action="{{action('Admin\UserController@insert')}}" method="post">

                    @csrf


                    <div class="form-row ">


                        <label>نام</label>
                        <input type="text" class="form-control {{  $errors->has('name') ? 'is-invalid' : '' }}"
                               placeholder="name" value="{{old('name')}}" name="name">

                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                    </div>


                    <div class="form-row ">


                        <label>نام خانوادگی</label>
                        <input type="text" class="form-control {{  $errors->has('family') ? 'is-invalid' : '' }} "
                               placeholder="family" value="{{old('family')}}" name="family">

                        <div class="invalid-feedback">
                            {{$errors->first('family')}}
                        </div>
                    </div>


                    <div class="form-row ">


                        <label>رمز</label>
                        <input type="text" class="form-control {{  $errors->has('password') ? 'is-invalid' : '' }}"
                               placeholder=" password" value="{{old('password')}}" name="password">

                        <div class="invalid-feedback">
                            {{$errors->first('password')}}
                        </div>
                    </div>

                    <br>
                    <div class="actions">
                        <button class="btn btn-success">save</button>
                        <a class="btn btn-light btn-lg"
                           href="{{ action('Admin\UserController@index') }}">Back</a>
                    </div>


                </form>
            </div>
        </div>
    </div>
@endsection