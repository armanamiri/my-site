@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    هشدار حذف کاربر
                </div>

                <div class="toolbar">

                </div>{{--خالی و بسته--}}

            </div>

            <div class="body">
                <form action="{{action('Admin\UserController@delete')}}" method="post">
                    <div class="panel-danger">
                        <div class="body">
                            @csrf

                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <div class="rtl">
                                هشدار
                                این فرآیند غیر قابل بازگشت می باشد
                            </div>
                            <br>
                        </div>
                    </div>

                    <br>
                    <div class="footer">
                        <div class="actions">
                            <button class="btn btn-danger btn-lg">حذف</button>
                            <a class="btn btn-light btn-lg"
                               href="{{ action('Admin\UserController@index') }}">Back</a>
                            {{-- URL::previous() یا این یا اون بالایی--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection