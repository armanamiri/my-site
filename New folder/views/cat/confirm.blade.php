@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    هشدار حذف دسته بندی
                </div>

                <div class="toolbar">

                </div>{{--خالی و بسته--}}

            </div>

            <div class="body">
                <form action="{{action('Admin\CatController@delete')}}" method="post">





                    <div class="panel-danger">
                        <div class="body">
                            @csrf

                            <input type="hidden" name="id" value="{{ $record->id }}">
                            <div class="rtl">
                                هشدار
                                این فرآیند قابل بازگشت نمی باشد
                            </div>
                            <br>
                        </div>
                    </div>

                    <br>
                    <div class="footer">
                        <div class="actions">
                            <button class="btn btn-danger btn-lg">حذف</button>
                            <a class="btn btn-light btn-lg"
                               href="{{ action('Admin\CatController@index') }}">Back</a>
                            {{-- URL::previous() یا این یا اون بالایی--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection