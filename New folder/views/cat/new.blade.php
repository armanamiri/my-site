@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    درج دسته بندی
                </div>

                <div class="toolbar">

                </div>{{--خالی و بسته--}}

            </div>

            <div class="body">
                <form action="{{action('Admin\CatController@insert')}}" method="post">
                    <div class="d-panel">
                        <div class="body">

                            @csrf

                            <div class="form-row ">
                                <label>نام کالا</label>
                                <input type="text"
                                       class="form-control form-control-lg  {{  $errors->has('name') ? 'is-invalid' : '' }}"
                                       placeholder="name" value="{{old('name')}}" name="name">
                                <div class="invalid-feedback">
                                    {{$errors->first('name')}}
                                </div>
                            </div>

                            <br>

                        </div>

                        <div class="footer">
                            <div class="actions">
                                <button class="btn btn-success btn-lg">Save</button>
                                <a class="btn btn-light btn-lg"
                                   href="{{ action('Admin\CatController@index') }}">Back</a>
                                {{-- URL::previous() یا این یا اون بالایی--}}
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection