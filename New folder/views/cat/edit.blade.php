@extends('layouts.admin')

@section('content')

    <div id="product-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    ویرایش دسته بندی
                </div>
                <div class="toolbar"></div>
            </div>

            <div class="body">

                @if( session()->has('updated'))
                    <div class="alert alert-success">succesful updated!</div>
                @endif

                <form action="{{action('Admin\CatController@update')}}" method="post">
                    <div class="d-panel">
                        <div class="body">


                            @csrf

                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{$product->id}}">



                            <div class="form-row ">
                                <label>دسته بندی </label>
                                <input type="text"
                                       class="form-control form-control-lg  {{  $errors->has('name') ? 'is-invalid' : '' }}"
                                       placeholder="name" value="{{old('name'  , $product->name)}}" name="name">
                                <div class="invalid-feedback">
                                    {{$errors->first('name')}}
                                </div>
                            </div>

                        </div>

                        <div class="footer">
                            <div class="actions">
                                <button class="btn btn-success btn-lg">save edit</button>
                                <a class="btn btn-light btn-lg"
                                   href="{{ action('Admin\CatController@index') }}">Back</a>
                                {{-- URL::previous() یا این یا اون بالایی--}}
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection