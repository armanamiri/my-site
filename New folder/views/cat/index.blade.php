@extends('layouts.admin')

@section('content')

    <div id="cat-index">
        <div class="page">
            <div class="header">
                <div class="title">
                    مدیریت دسته بندی ها
                </div>
                <div class="toolbar">
                    <a href="{{ action('Admin\CatController@new') }}" class="btn btn-success">درج دسته بندی جدید</a>
                </div>
            </div>


            @if( session()->has('has-products'))
                <div class="alert alert-warning">هشدار
                    دسته بندی که محصول دارد قابل حذف شدن نیست.</div>
            @endif




            <div class="body">
                @if( session()->has('deleted'))
                    <div class="alert alert-success">succesful delete! </div>
                @endif

                @if( session()->has('inserted'))
                    <div class="alert alert-success">succesful inserted!</div>
                @endif
                <table border="2px" class="table table-dark table-striped table-hover">
                    <tr>
                        <th>عملیات</th>
                        <th>تعداد کالا ها</th>
                        <th>نام</th>
                        <th>#</th>
                    </tr>

                    @foreach( $records as $record)
                        <tr>
                            <td>
                                <a href="{{ action('Admin\CatController@confirm' ,['id'=>$record->id] ) }}"
                                   class="btn btn-sm btn-danger">del</a>
                                <a href="{{ action('Admin\CatController@edit' ,['id'=>$record->id] ) }}"
                                   class="btn btn-sm btn-warning">edit</a>
                            </td>
                            <td>{{$record->products()->count() }}</td>
                            <td>{{$record->name}}</td>
                            <td>{{ $loop->index + $records->firstItem()}}</td>
                        </tr>
                    @endforeach

                </table>
                {{$records->links()}}

            </div>
        </div>
    </div>

@endsection